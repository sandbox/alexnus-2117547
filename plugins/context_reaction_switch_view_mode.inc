<?php


class ContextReactionSwitchViewMode extends context_reaction {
  
  /**
   * Editor form.
   */
  function editor_form($context) {
    $form = $this->options_form($context);

    // Hide descriptions which take up too much space.
    unset($form['title']['#description']);
    unset($form['subtitle']['#description']);
    unset($form['class']['#description']);
    return $form;
  }

  /**
   * Submit handler for editor form.
   */
  function editor_form_submit($context, $values) {
    return $values;
  }

  /**
   * Allow admins to provide a section title, section subtitle and section class.
   */
  function options_form($context) {
    $values = $this->fetch_from_context($context);
    $entities = field_info_bundles();
    //dpm($values);
    
    $form = array(
      '#tree' => TRUE,
      '#title' => 'View mode context',
    );
    foreach($entities as $entity => $bundles){
      $form[$entity] = array(
        '#type' => 'fieldset',
        '#title' => $entity,
        '#collapsed' => true,
        '#collapsible' => true
      );
      foreach($bundles as $bundle => $items){
        $form[$entity][$bundle] = array(
          '#type' => 'fieldset',
          '#title' => $bundle,
          '#collapsed' => true,
          '#collapsible' => true
        );
        
        $form[$entity][$bundle]['view_modes'] = array(
          '#type' => 'options',
          '#title' => t('View modes equivalence'),
          //'#description' => 'Specify any additional attributes here',
          '#optgroups' => FALSE,
          '#multiple' => FALSE,
          '#key_type' => 'custom',
          '#key_type_toggle' => FALSE,
          '#key_type_toggled' => TRUE,
          '#default_value_allowed' => FALSE,
          '#options' => isset($values[$entity][$bundle]['view_modes']['options']) ? $values[$entity][$bundle]['view_modes']['options'] : array(),
          '#element_validate' => array('form_options_validate', 'context_view_mode_options_element_value'),
          '#default_value' => NULL,
        );
      }
    }
    return $form;
  }
  
  function execute(&$build){
    $contexts = $this->get_contexts();
    if(!empty($contexts)){
      foreach($contexts as $context){
        if(!empty($build['#bundle']) && !empty($context->reactions['context_view_mode'][$build['#entity_type']][$build['#bundle']])){
          $options = $context->reactions['context_view_mode'][$build['#entity_type']][$build['#bundle']]['view_modes']['options'];
          foreach($options as $k => $v){
            if($k == $build['#view_mode']) $build['#view_mode'] = $v;
          }
        }
      }
    }
  }

}